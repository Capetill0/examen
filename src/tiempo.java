

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;

public class tiempo extends Thread{
    
    private DateFormat df= new SimpleDateFormat("hh:mm:ss aa");
    private JLabel label;

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }
    
    @Override
    public void run(){
        while(true){
            try {
                this.getLabel().setText(df.format(new Date()).toString());
                
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}